package SumGroups;

import org.junit.Assert;
import org.junit.Test;

public class SolutionTest {
    @Test
    public void basicTests() {
        Solution solution = new Solution();
        Assert.assertEquals(6, solution.sumGroups(new int[] {2, 1, 2, 2, 6, 5, 0, 2, 0, 5, 5, 7, 7, 4, 3, 3, 9}));
        Assert.assertEquals(5, solution.sumGroups(new int[] {2, 1, 2, 2, 6, 5, 0, 2, 0, 3, 3, 3, 9, 2}));
        Assert.assertEquals(1, solution.sumGroups(new int[] {2}));
        Assert.assertEquals(2, solution.sumGroups(new int[] {1,2}));
        Assert.assertEquals(1, solution.sumGroups(new int[] {1,1,2,2}));
    }
}
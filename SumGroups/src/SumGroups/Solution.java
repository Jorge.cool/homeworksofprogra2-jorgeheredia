package SumGroups;

/**
 * <h1> Task </h1>
 * Given an array of integers, sum consecutive even numbers and consecutive odd numbers.
 * Repeat the process while it can be done and return the length of the final array.
 *
 * @author JorgeHeredia
 */
public class Solution {

    /**
     * This method adds the numbers as long as possible.
     * Because your goal is to reduce the array as much as possible to return its length.
     *
     * @param arr array of numbers
     * @return length of array
     */
    public int sumGroups(int[] arr) {
        while (isPossibleSumGroups(arr)) {
            arr = sumNumberWithSameParity(arr);
        }

        return arr.length;
    }

    /**
     * This method adds the numbers if their parity is equals.
     *
     * @param numbers array with numbers.
     * @return array with sums of group
     */
    public int[] sumNumberWithSameParity(int[] numbers) {
        int[] addedNumbers = new int[numbers.length];

        int count = 0;
        addedNumbers[count] = numbers[count];
        count++;

        for (int i = 1; i <= numbers.length - 1; i++) {

            if (i == numbers.length - 1) {
                if (sameParity(numbers[i], numbers[i - 1])) {
                    count -= (i - 1 == 0) ? 1 : 0;
                    addedNumbers[count] += numbers[i];
                } else {
                    addedNumbers[count] = numbers[i];
                }
            } else if (sameParity(numbers[i], numbers[i + 1])) {
                addedNumbers[count] += numbers[i];
            } else if (sameParity(numbers[i], numbers[i - 1])) {
                count -= (i - 1 == 0) ? 1 : 0;
                addedNumbers[count] += numbers[i];
                count++;
            } else {
                addedNumbers[count] = numbers[i];
                count++;
            }
        }

        return resizeArray(addedNumbers, count);
    }

    /**
     * This method compares the parity of two numbers.
     *
     * @param num1 any number
     * @param num2 any number
     * @return boolean (true if parity is equals, false if parity is different)
     */
    public boolean sameParity(int num1, int num2) {
        return isEvenNumber(num1) == isEvenNumber(num2);
    }

    /**
     * This method return if is or not an even number
     *
     * @param number any number
     * @return boolean (true = even, false = odd)
     */
    public boolean isEvenNumber(int number) {
        return number % 2 == 0;
    }

    /**
     * This method resize length of array.
     *
     * @param numbers array
     * @param size new length
     * @return new array
     */
    public int[] resizeArray(int[] numbers, int size) {
        int[] newArray = new int[size + 1];
        for (int i = 0; i < size + 1; i++) {
            newArray[i] = numbers[i];
        }
        return newArray;
    }

    /**
     * This method verify if is possible to add.
     *
     * @param numbers array with numbers
     * @return boolean (is possible or not)
     */
    public boolean isPossibleSumGroups (int[] numbers) {
        boolean isPossible = false;
        if (numbers.length != 1) {
            for (int i = 0; i < numbers.length - 1; i++) {
                if (sameParity(numbers[i], numbers[i + 1])) {
                    isPossible = true;
                }
            }
        }
        return isPossible;
    }
}

package SimpleDrawingBoard;

import java.util.ArrayList;

/**
 * This is canvas class
 * A board where we can draw lines or rectangles.
 *
 * @author JorgeHeredia
 */
public class Canvas {
    private int width, height;
    ArrayList<String> rows;

    //
    int BORDERS_NOT_CONSIDERING = 1;

    /**
     * Constructor method.
     * Generate a canvas empty with these measurements.
     *
     * @param width width of board
     * @param height height of board
     */
    public Canvas(int width, int height) {
        this.width = width;
        this.height = height;
        generateCanvasEmpty();
    }

    /**
     * This method generate canvas empty with measurements of object.
     */
    public void generateCanvasEmpty() {
        rows = new ArrayList<>();
        String borderTopAndBottom = "-".repeat(width+2);
        String borderLeftAndRight = "|" + " ".repeat(width) + "|\n";

        rows.add(borderTopAndBottom + "\n");
        for (int i = 0; i < height; i++) {
            rows.add(borderLeftAndRight);
        }
        rows.add(borderTopAndBottom);
    }

    /**
     * This method draw lines or rectangle.
     * According to two coordinates.
     *
     * <p>
     *     For example, if coordinates in x or y match. Draw a line.
     *
     *     But if coordinates not match.
     *     Generate rectangle starting in first coordinate and ending in second coordinate.
     * </p>
     *
     * @param x1 coordinate in x of first pair
     * @param y1 coordinate in y of first pair
     * @param x2 coordinate in x of second pair
     * @param y2 coordinate in y of second pair
     * @return canvas(object)
     */
    public Canvas draw(int x1, int y1, int x2, int y2) {
        if (x1 == x2) {
            String rowDrawing = "|" + " ".repeat(x1) + "x" + " ".repeat(width - x1 - BORDERS_NOT_CONSIDERING) + "|\n";
            for (int i = BORDERS_NOT_CONSIDERING + y1; i < rows.size() - BORDERS_NOT_CONSIDERING; i++) {
                rows.remove(i);
                rows.add(i, rowDrawing);

                if (i == y2 + BORDERS_NOT_CONSIDERING) {
                    i += y2;
                }
            }
        }
        else if (y1 == y2) {
            String rowDrawing = "|" + " ".repeat(x1) + "x".repeat(x2 + BORDERS_NOT_CONSIDERING - x1) + " ".repeat(width - (x2 + BORDERS_NOT_CONSIDERING)) + "|\n";
            rows.remove(y1 + BORDERS_NOT_CONSIDERING);
            rows.add(y1 + BORDERS_NOT_CONSIDERING, rowDrawing);

        }
        else {
            String rowTopAndBottom = "|" + " ".repeat(x1) + "x".repeat(x2 + BORDERS_NOT_CONSIDERING - x1) +
                    " ".repeat(width - (x2 + BORDERS_NOT_CONSIDERING)) + "|\n";
            rows.remove(y1 + BORDERS_NOT_CONSIDERING);
            rows.add(y1 + BORDERS_NOT_CONSIDERING, rowTopAndBottom);
            for (int i = y1 + BORDERS_NOT_CONSIDERING; i < y2; i++) {
                String rowDrawing = "|" + " ".repeat(x1) + "x" +
                        " ".repeat(x2 - x1 - BORDERS_NOT_CONSIDERING) + "x" +
                        " ".repeat(width - x2 - BORDERS_NOT_CONSIDERING) + "|\n";
                rows.remove(i + BORDERS_NOT_CONSIDERING);
                rows.add(i + BORDERS_NOT_CONSIDERING, rowDrawing);
            }
            rows.remove(y2 + BORDERS_NOT_CONSIDERING);
            rows.add(y2 + BORDERS_NOT_CONSIDERING, rowTopAndBottom);

        }
        return this;
    }

    /**
     * LOGIC NOT ENDED
     * This method will work to fill a rectangle or the board.
     * Like "bucket fill" of photoshop or paint.
     *
     * @param x coordinate in x axis
     * @param y coordinate in y axis
     * @param ch char to fill
     * @return canvas(Object)
     */
    public Canvas fill(int x, int y, char ch) {
        boolean isRectangle = false;

        for (int i = y + BORDERS_NOT_CONSIDERING; i < rows.size(); i++){
            String row = rows.get(i);
            String[] charactersOfRow = row.split("x");
            try {
                String fill = String.valueOf(ch);
                String spaceSelectedForFill = "x" + fill.repeat(charactersOfRow[1].length()) + "x";
                charactersOfRow[1] = spaceSelectedForFill;
                rows.remove(i);
                rows.add(i, charactersOfRow.toString());
            } catch (java.lang.IllegalArgumentException e) {
                System.out.println("Error aXD");
            }
        }
        return this;
    }

    /**
     * This method convert arraylist of rows (strings) in one string.
     *
     * @return canvas(String)
     */
    public String drawCanvas() {
        String canvas = "";

        for (String row : rows) {
            canvas += row;
        }

        return canvas;
    }
}

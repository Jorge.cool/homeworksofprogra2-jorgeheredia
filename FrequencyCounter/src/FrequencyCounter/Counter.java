package FrequencyCounter;

/**
 * This counter class.
 * Finds count of most frequent item in an Array.
 *
 * @author JorgeHeredia
 */
public class Counter {
    private int numberHigher;
    private int[] numbers;

    /**
     * Constructor method receive array because is easier calls to methods
     * without passing this param and this param is of the object.
     *
     * @param numbers array of ints
     */
    public Counter(int[] numbers) {
        this.numbers = numbers;
    }

    /**
     * This method iterates all numbers and searches what item is the most frequent.
     *
     * @return the number most frequent
     */
    public int searchMostFrequent() {
        numberHigher = numbers[0];
        for (int i = 1; i < numbers.length - 1; i++) {
            if (countFrequency(numberHigher) < countFrequency(numbers[i])) {
                numberHigher = numbers[i];
            }
        }

        return numberHigher;
    }

    /**
     * This method receives a number and counts how often that number appears..
     *
     * @param number int to count
     * @return frequency or appears of that number
     */
    public int countFrequency(int number) {
        int frequency = 0;

        for (int i = 0; i < numbers.length; i++) {
            if (number == numbers[i]) {
                frequency++;
            }
        }

        return frequency;
    }

    /**
     * This method generates a string with the message of the number most frequent and its frequency,
     *
     * @return "Number: x \n Frequency: y"
     */
    public String FrequencyOfMostFrequent() {
        searchMostFrequent();
        return "Number: " + numberHigher + "\nFrequency: " + countFrequency(numberHigher);
    }
}

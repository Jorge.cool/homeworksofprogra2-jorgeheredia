package FrequencyCounter;

import junit.framework.TestCase;
import org.junit.Test;


public class testCounter {
    public Counter counterDefault() {
        int[] numbers = {3, -1, -1, -1, 2, 3, -1, 3, -1, 2, 4, 9, 3};
        Counter c = new Counter(numbers);

        return c;
    }

    @Test
    public void countFrequency() {
        TestCase.assertEquals(4, counterDefault().countFrequency(3));
    }

    @Test
    public void searchMostFrequent() {
        TestCase.assertEquals(-1, counterDefault().searchMostFrequent());
    }

    @Test
    public void showMostFrequent() {
        TestCase.assertEquals("Number: -1\nFrequency: 5", counterDefault().FrequencyOfMostFrequent());
    }
}

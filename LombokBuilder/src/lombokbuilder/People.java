package lombokbuilder;

public class People{
    private int age;
    private String name;
    private String lastName;
    final String GREET="hello";

    public People(int age, String name, String lastName) {
        this.age = age;
        this.name = name;
        this.lastName = lastName;
    }

    public String greet(){
        return GREET + " my name is " + getName();
    }

    public String getName() {
        return name;
    }

    public static void main(String[] args) {
        People people1 = new People(18, "Jorge", "Heredia");
        People people2 = new People(22, "Gabriela", "Becerra");

        System.out.println(people1.greet());
        System.out.println(people2.greet());
    }
}

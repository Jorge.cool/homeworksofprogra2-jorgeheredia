package lombokbuilder;

import org.junit.Test;
import static org.junit.Assert.*;


public class PeopleTest {

    @Test
    public void testSample() {
        People person = new People(25, "Adam", "Savage");
        assertEquals(
                "Adam",
                person.getName()
        );
        assertEquals(
                "hello my name is Adam",
                person.greet()
        );
    }
}
    
package RotateArray;

/**
 * Rotator class use an object array and a number.
 * Rotates array for left or right depends on number is positive or negative
 *
 * @author JorgeHeredia
 */
public class Rotator {

    /**
     * Rotate method check if number of rotation is positive or negative and decide if rotate for left or right,
     * @param data array of objects
     * @param n number of rotation
     * @return Object array
     */
    public Object[] rotate(Object[] data, int n) {
        Object objAux;
        int arrSize = data.length - 1;

        if (n > 0) {
            for (int numRepetitions = 0; numRepetitions < n; numRepetitions++) {
                objAux = data[arrSize];
                for (int i = arrSize; i >= 0 ;i--) {
                    data[i] = (i == 0) ? objAux : data[i - 1];
                }
            }
        } else {
            for (int numRepetitions = 0; numRepetitions > n; numRepetitions--) {
                objAux = data[0];
                for (int i = 0; i <= arrSize; i++) {
                    data[i] = (i == arrSize) ? objAux : data[i + 1];
                }
            }
        }

        return data;
    }
}
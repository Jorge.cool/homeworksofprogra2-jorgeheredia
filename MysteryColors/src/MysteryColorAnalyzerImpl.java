import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of mysteryColorAnalyzer interface
 *
 * @see MysteryColorAnalyzer
 * @author JorgeHeredia
 */
public class MysteryColorAnalyzerImpl implements MysteryColorAnalyzer {

    /**
     * This method will determine how many distinct colors are in the given list
     *
     * <p>
     *     Checks if iterated color of the mysteryColors list it's in nonRepeatingColors list.
     *     If it's not in list, this element is added. Finally get and return size nonRepeatingColors list.
     * </p>
     *
     * @param mysteryColors list of colors from which to determine the number of distinct colors
     * @return number of distinct colors
     */
    @Override
    public int numberOfDistinctColors(List<Color> mysteryColors) {
        ArrayList<Color> nonRepeatingColors = new ArrayList<>();

        for (Color color : mysteryColors) {
            if (!nonRepeatingColors.contains(color)){
                nonRepeatingColors.add(color);
            }
        }

        return nonRepeatingColors.size();
    }

    /**
     * This method will count how often a specific color is in the given list
     *
     * @param mysteryColors list of colors from which to determine the count of a specific color
     * @param color color to count
     * @return number of occurrence of the color in the list
     */
    @Override
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        int occurrenceOfColor = 0;
        for (Color colorIterated : mysteryColors) {
            if (color == colorIterated) {
                occurrenceOfColor++;
            }
        }
        return occurrenceOfColor;
    }
}

import java.util.ArrayList;

public class TestMysteryColorAnalyzer {
    public static void main(String[] args) {
        MysteryColorAnalyzerImpl mysteryColorAnalyzer = new MysteryColorAnalyzerImpl();

        ArrayList<Color> mysteryColors = new ArrayList<>();
        mysteryColors.add(Color.BLUE);
        mysteryColors.add(Color.RED);
        mysteryColors.add(Color.BLUE);
        mysteryColors.add(Color.YELLOW);

        System.out.println("Distinct Colors: " + mysteryColorAnalyzer.numberOfDistinctColors(mysteryColors) + "\n");
        System.out.println("Occurrence: " + mysteryColorAnalyzer.colorOccurrence(mysteryColors,Color.BLUE));
    }
}
